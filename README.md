# Enable Financing Widget Integration Guide

## Introduction

This guide provides step-by-step instructions on how to embed the Enable Financing Widget into a website for loan application flow. The Enable Financing Widget is a JavaScript library that allows seamless integration of loan application functionality into your website.

<p align="center">
  <img src="./images/widget.png" alt="Nature" width="250" />
</p>


### Prerequisites

- Access to the Enable Financing Widget library: https://yourslug.demo.weenablefinancing.com/widget.js
- Familiarity with HTML and JavaScript

### Integration Steps

1. **Include the Widget Script:** Add the following script tag just before the closing body tag (`</body>`) in your HTML file:
```
<script src="https://yourslug.demo.weenablefinancing.com/widget.js"></script>
```

2. **Integration with React:** If you are working with React, please download the example code available in the **react** folder. This code showcases the implementation of all the available functions.

3. **Usage:** The `Enable.setup` function initializes the widget. Make sure to replace the parameters with your own values.


### Available Functions

**Enable.setup** - This function is called once to load the Widget inside the specified div element.
```
Enable.setup('clientKey', 'widgetId', (applicationData) => {
  console.log(applicationData);
}, formData);
```
- Replace `clientKey` with the key provided by Enable.
- Replace `widgetId` with the ID of the div where the loan application form will be loaded.
- Replace `(applicationData) => console.log(applicationData)` with your implementation of a callback function (optional).
  - Provide a callback function to receive the application ID and visitor token. After the user submits their Personal Information, this callback function will be triggered, sending back the application ID and visitor token.
  - Use this information to access an existing application. Note that the user will need to provide their date of birth to retrieve the application data.
  - If you don't require a callback function, set it to `null` in the Enable.setup call: `Enable.setup('clientKey', 'widgetId', null);` By passing null as the third argument, you indicate that no callback function is needed in this setup.
- If the fourth argument (`formData`) includes object data, the form initializes with pre-filled information. Refer to the table below for accepted properties and their corresponding data types.

**Enable.newApp** - This function initiates a new loan application, allowing for both blank forms or pre-filled data.
- To start a new application with a blank form:
```
Enable.newApp();
```
- To start a new application with pre-filled data:
```
Enable.newApp(data);
```
- **data** must be a JSON object, and the following properties can be used to populate the loan application form:

| Property name | Data type | Description | Example |
|----------|----------|----------|----------|
| loan_amount | `number` | Loan amount | `50000` |
| first_name | `string` | Applicant's first name (max 200 characters) | `John` |
| last_name | `string` | Applicant's last name (max 200 characters) | `Doe` |
| education_level | `string` | Applicant's education level (accepted values are: `HIGH_SCHOOL_DIPLOMA`, `ASSOCIATES_DEGREE`, `BACHELORS_DEGREE`, `MASTERS_DEGREE`, `CERTIFICATE`, `STILL_ENROLLED`, `OTHER_GRADUATE_DEGREE`, or `OTHER`) | `BACHELORS_DEGREE` |
| email | `string` | Applicant's email address (max 255 characters) | `john.doe@example.com` |
| phone_number | `string` | Applicant's phone (format `(DDD) DDD-DDDD`) | `(555) 555-5555` |
| address1 | `string` | Address line 1 containing the full street address with unit, apt number, etc. (max 200 characters) | `999 S My Street Dr Unit B` |
| city | `string` | City (max 200 characters) | `Draper` |
| state | `string` | State (max 2 characters) | `UT` |
| postal_code | `string` | Postal code (max 5 characters) | `84045` |
| property_status | `string` | Property status (accepted values are: `MORTGAGE`, `OWN`, or `RENT`) | `MORTGAGE` |
| employment_status | `string` | Employment status (accepted values are: `FULLTIME`, `PARTIME`, `MILITARY`, `NOT_EMPLOYED`, `SELF_EMPLOYED`, `RETIRED`, or `OTHER`) | `RETIRED` |
| pay_frequency | `string` | Pay frequency (accepted values are: `WEEKLY`, `BIWEEKLY`,`SEMI_MONTHLY`, or `MONTHLY`) | `MONTHLY` |
| annual_income | `number` | Applicant's annual income before taxes | `65000` |
| credit_rating | `string` | Applicant's self reported credit score (accepted values are: `GREATHER_THAN_719`, `V660_719`, `V620_659`, `LESS_THAN_620`, or `LIMITED`) | `V620_659` |

Ensure that the provided data adheres to the specified data types and formats for accurate form population.

**Enable.open** - If a callback was provided, this function is used to open an existing application. For security reasons, the applicant needs to validate their date of birth.
```
Enable.open('applicationId', 'vuid');
```
- Replace `applicationId` and `vuid` with the values received from the callback function. Ensure that these values are accurately provided for the targeted application.

## Conclusion
By following these steps, you can easily integrate the Enable Financing Widget into your website, allowing users to initiate loan applications seamlessly. Customize the provided examples according to your application's requirements.

**More code examples to come**