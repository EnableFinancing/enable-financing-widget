"use client";
import styles from "./page.module.css";
import { useEffect, useState } from 'react';

export default function Home() {
  const [applicationData, setApplicationData] = useState<any>(null);

  useEffect(() => {
    const enableScript = document.getElementById('enable-script')

    if (enableScript && !window.Enable) {
      enableScript.onload = () => {
        window.Enable.setup("1bbf91a3-c85c-4852-a095-74dc787d88c2", "load-widget-here", saveApplicationInfo);
      };
    } else if (window.Enable) {
      window.Enable.setup("1bbf91a3-c85c-4852-a095-74dc787d88c2", "load-widget-here", saveApplicationInfo);
    }
  }, []);

  const saveApplicationInfo = (appData: any) => {
    setApplicationData(appData);
  } 

  const prefillForm = () => {
    setApplicationData(null);
    const data = {
      loan_amount: 6500,
      loan_purpose: "OTHER",
      external_id: "48327423749724",
      first_name: "John",
      last_name: "Doe",
      education_level: "BACHELORS_DEGREE"
    }
    window.Enable.newApp(data);
  }

  const startNew = () => {
    setApplicationData(null);
    window.Enable.newApp();
  }

  const openApplication = () => {
    window.Enable.open(applicationData?.applicationId, applicationData?.vuid);
  }

  return (
    <div className={styles.container}>
      <div className={`${styles.box} ${styles.padding}`}>
        <input type="button" value="Pre fill form" onClick={prefillForm} className={styles.button} /><br />
        <input type="button" value="Start new application" onClick={startNew} className={styles.button} /><br />
        
        {applicationData && <div>
          <hr />
          <h4>Application Id</h4>
          <p>{applicationData.applicationId}</p>
          <h4>Visitor token</h4>
          <p>{applicationData.vuid}</p>
          <input type="button" value={`Open application ${applicationData.applicationId}`} onClick={openApplication} className={styles.button} /><br />
        </div>}

      </div>
      <div className={styles.widget} id="load-widget-here"></div>
    </div>
  );
}
