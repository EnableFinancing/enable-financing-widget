import { useEffect, useState } from "react";
import "./App.css";

function App() {
  const [applicationData, setApplicationData] = useState(null);

  useEffect(() => {
    window.Enable.setup("1bbf91a3-c85c-4852-a095-74dc787d88c2", "load-widget-here", saveApplicationInfo, { loan_amount: 6500 });
  }, []);

  const saveApplicationInfo = (appData) => {
    setApplicationData(appData);
  } 

  const prefillForm = () => {
    setApplicationData(null);
    const data = {
      loan_amount: 6500,
      loan_purpose: "OTHER",
      external_id: "48327423749724",
      first_name: "John",
      last_name: "Doe",
      education_level: "BACHELORS_DEGREE"
    }
    window.Enable.newApp(data);
  }

  const startNew = () => {
    setApplicationData(null);
    window.Enable.newApp();
  }

  const openApplication = () => {
    window.Enable.open(applicationData.applicationId, applicationData.vuid);
  }

  return (
    <div className="container">
      <div className="box padding">
        <input type="button" value="Pre fill form" onClick={prefillForm} /><br />
        <input type="button" value="Start new application" onClick={startNew} /><br />
        
        {applicationData && <div>
          <hr />
          <h4>Application Id</h4>
          <p>{applicationData.applicationId}</p>
          <h4>Visitor token</h4>
          <p>{applicationData.vuid}</p>
          <input type="button" value={`Open application ${applicationData.applicationId}`} onClick={openApplication} /><br />
        </div>}

      </div>
      <div className="widget" id="load-widget-here"></div>
    </div>
  );
}

export default App;
